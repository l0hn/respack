package respack

import (
	"unsafe"
	"io/ioutil"
	"os"
	"path/filepath"
	"compress/flate"
	"bytes"
	"io"
	"strings"
	"encoding/json"
)

type Respack struct {
	ResMap map[string][]byte
}

func uncompress(compressed []byte) (uncompressed []byte, err error)  {
	byteReader := bytes.NewReader(compressed)
	data := bytes.Buffer{}
	zipReader := flate.NewReader(byteReader)
	defer zipReader.Close()
	_, err = io.Copy(&data, zipReader)

	if err != nil {
		return
	}

	uncompressed = data.Bytes()
	return
}

func compress(source []byte) (compressed []byte, err error)  {
	byteReader := bytes.NewReader(source)
	data := bytes.Buffer{}
	zipWriter, err := flate.NewWriter(&data, 9)

	if err != nil {
		return
	}

	_, err = io.Copy(zipWriter, byteReader)
	err = zipWriter.Close()

	if err != nil {
		return
	}

	compressed = data.Bytes()
	return
}

func cBufToArray(buffer unsafe.Pointer, size uint) []byte {
	return (*[1 << 30]byte)(buffer)[:size:size]
}

func NewRespack() *Respack {
	return &Respack{
		ResMap: make(map[string][]byte),
	}
}

func (respack *Respack) InitFromFile(filename string) error {
	compressed, err := ioutil.ReadFile(filename)

	if err != nil {
		return err
	}

	data, err := uncompress(compressed)

	if err != nil {
		return err
	}

	err = respack.unmarshal(data)
	return err
}

func (respack *Respack) PackToFile(filename string) error {
	data, err := respack.marshal()

	if err != nil {
		return err
	}

	compressed, err := compress(data)

	if err != nil {
		return err
	}

	return ioutil.WriteFile(filename, compressed, 0644)
}

func (respack *Respack) marshal() (data []byte, err error) {
	return json.Marshal(respack.ResMap)
}

func (respack *Respack) unmarshal(data []byte) error {
	return json.Unmarshal(data, &respack.ResMap)
}

func (respack *Respack) GetResource(key string) (data []byte, ok bool) {
	data, ok = respack.ResMap[strings.Replace(key, "\\", "/", -1)]
	return
}

func (respack *Respack) SetResource(key string, data []byte) {
	respack.ResMap[strings.Replace(key, "\\", "/", -1)] = data
}

func (respack *Respack) SetDataFromDir(dir string) error {
	absDir, err := filepath.Abs(dir)

	if err != nil {
		return err
	}

	trimCount := len(absDir)

	err = filepath.Walk(absDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			_, ignoreErr := os.Stat(filepath.Join(path, ".respackignore"))
			if ignoreErr == nil {
				return filepath.SkipDir
			}

			return nil
		}

		dat, err := ioutil.ReadFile(path)

		if err != nil {
			return err
		}

		key := filepath.Base(absDir) + "/" + strings.Replace(strings.Trim(path[trimCount:], "\\/"), "\\", "/", -1)

		respack.ResMap[key] = dat

		return nil
	})

	return err
}