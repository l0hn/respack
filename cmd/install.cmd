SET WORK_DIR=%~dp0%
set CC=x86_64-w64-mingw32-gcc
set GOARCH=amd64
set GOOS=windows
set CGO_ENABLED=1
go build -a -ldflags "-w -s" -o %WORK_DIR%respacker.exe %WORK_DIR%respacker.go
copy /Y %WORK_DIR%respacker.exe %gopath%\bin\respacker.exe
