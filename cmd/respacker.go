package main


import (
	"flag"
	"fmt"
	"os"
	"io/ioutil"
	"log"
	"gitlab.com/l0hn/respack"
)



func main() {
	outFile := flag.String("out", "respack.pack", "optional - output file (path to an existing exe when using -embed)")
	resourceDir := flag.String("source", "", "optional - path to a directory containing resources to pack")
	embed := flag.Bool("embed", false, "optional - embeds the respack content to an existing executable file")
	manifest := flag.String("manifest", "", "optional - path to an application manifest file to be embedded (must be used with -embed)")

	flag.Parse()

	if len(*resourceDir) == 0 && len(*manifest) == 0 || (len(*manifest) > 0 && !*embed)  {
		fmt.Printf("Usage of %s:\n\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}

	if *embed && len(*manifest) != 0 {
		packManifest(*outFile, *manifest)
	}

	if len(*resourceDir) > 0 {
		packRes(*outFile, *resourceDir, *embed)
	}
}

func packManifest(outFile string, manifestFile string) {
	data, err := ioutil.ReadFile(manifestFile)
	if err != nil {
		log.Println(err.Error())
		os.Exit(1)
	}

	manifest := string(data[:])
	err = respack.EmbedManifest(outFile, manifest)

	if err != nil {
		log.Println(err.Error())
		os.Exit(1)
	}
}

func packRes(outFile string, resourceDir string, embed bool) {
	r := respack.NewRespack()
	err := r.SetDataFromDir(resourceDir)

	if err != nil {
		println(err)
		os.Exit(1)
	}

	if embed {
		err = r.EmbedIntoExe(outFile)
	} else {
		err = r.PackToFile(outFile)
	}

	if err != nil {
		println(err)
		os.Exit(1)
	}

	//println("verifying data..")

	vr := respack.NewRespack()

	if embed {
		err = vr.InitFromExe(outFile)
	} else {
		err = vr.InitFromFile(outFile)
	}

	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	for k,v := range r.ResMap {
		//fmt.Printf("checking %s.. ", k)
		dat, ok := vr.ResMap[k]
		if !ok {
			fmt.Printf("ERROR: could not find resource %s in target\n", k)
			os.Exit(1)
		}

		if len(dat) != len(v) {
			fmt.Printf("ERROR: size mismatch for resource %s [source=%d, target=%d]\n", k, len(v), len(dat))
			os.Exit(1)
		}
	}
}
