package respack

/*
#ifdef _WIN32
#include <windows.h>
#include <stdio.h>

int resPackAddResource(LPCSTR pFileName, LPCSTR pResourceName, LPCSTR lpType, void* data, u_int size) {
	BOOL result = TRUE;
	HMODULE hModule = NULL;

	hModule = BeginUpdateResource(pFileName, FALSE);

	if (hModule != NULL) {
		result = UpdateResource(
			hModule,
			lpType,
			pResourceName,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPVOID)data,
			size
		);
	}

	if (result) {
		result = EndUpdateResource(hModule, FALSE);
	}

	//cleanup
	if (hModule != NULL) {
		FreeLibrary(hModule);
	}

	if (!result) {
		return GetLastError();
	}

	return ERROR_SUCCESS;
}

HMODULE getCurrentModule() {
	HGLOBAL hResource = NULL;
	HMODULE hModule = NULL;
	DWORD flags = GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT | GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS;
	GetModuleHandleEx(flags, (LPCSTR)getCurrentModule, &hModule);
	return hModule;
}

int resPackGetResourceEmbedded(LPCSTR pResourceName, void** lpdata, u_int* size) {
	HMODULE hModule = NULL;
	HGLOBAL hResource = NULL;
	HRSRC hResourceInfo = NULL;
	u_int hSize = 0;
	DWORD result = ERROR_SUCCESS;

	hModule = getCurrentModule();
	hResourceInfo = FindResource(hModule, pResourceName, RT_RCDATA);

	if (hResourceInfo == NULL) {
		result = GetLastError();
	} else {
		hSize = SizeofResource(hModule, hResourceInfo);
	}

	if (result == ERROR_SUCCESS && hSize == 0) {
		result = GetLastError();
	}

	if (lpdata != NULL && hSize > 0) {
		hResource = LoadResource(hModule, hResourceInfo);

		if (hResource == NULL) {
			result = GetLastError();
		} else {
			LPVOID lpAddress = (LPBYTE) LockResource(hResource);
			memcpy(*lpdata, lpAddress, hSize);
		}
	}

	*size = hSize;

	FreeLibrary(hModule);

	return result;
}

int resPackGetResource(LPCSTR pFileName, LPCSTR pResourceName, void** lpdata, u_int* size) {
	DWORD result = ERROR_SUCCESS;
	HRSRC hResourceInfo = NULL;
	HMODULE hModule = NULL;
	HGLOBAL hResource = NULL;
	u_int hSize = 0;

	hModule = LoadLibraryEx(pFileName, NULL, LOAD_LIBRARY_AS_DATAFILE);

	if (hModule == NULL) {
		result = GetLastError();
	} else {
		hResourceInfo = FindResource(hModule, pResourceName, RT_RCDATA);
	}

	if (result == ERROR_SUCCESS && hResourceInfo == NULL) {
		result = GetLastError();
	} else {
		hSize = SizeofResource(hModule, hResourceInfo);
	}

	if (result == ERROR_SUCCESS && hSize == 0) {
		result = GetLastError();
	}

	if (result == ERROR_SUCCESS && lpdata != NULL) {
		hResource = LoadResource(hModule, hResourceInfo);
		if (hResource == NULL) {
			result = GetLastError();
		} else {
			LPVOID lpAddress = LockResource(hResource);
			memcpy(*lpdata, lpAddress, hSize);
		}

		if (*lpdata == NULL) {
			result = GetLastError();
		}
	}

	*size = hSize;

	//cleanup
	if (hModule != NULL) {
		FreeLibrary(hModule);
	}

	return result;
}
#endif
*/
import "C"
import (
	"unsafe"
	"fmt"
)

func (respack *Respack) InitFromExe(filename string) error {
	size := C.uint(0)
	resName := C.CString("respack")
	defer C.free(unsafe.Pointer(resName))
	filenameC := C.CString(filename)
	defer C.free(unsafe.Pointer(filenameC))

	result := uint(C.resPackGetResource(filenameC, resName, nil, &size))

	if result != 0 {
		return fmt.Errorf("failed to load resource with error %#x", result)
	}

	buf := C.malloc(C.size_t(size))
	defer C.free(buf)

	result = uint(C.resPackGetResource(filenameC, resName, &buf, &size))

	if result != 0 {
		return fmt.Errorf("failed to load resource with error %#x", result)
	}

	compressedData := cBufToArray(buf, uint(size))

	data, err := uncompress(compressedData)
	if err != nil {
		return err
	}

	return respack.unmarshal(data)
}

//initialise content from the respack resource file
func (respack *Respack) InitEmbedded() error {
	size := C.uint(0)
	resName := C.CString("respack")
	defer C.free(unsafe.Pointer(resName))

	result := uint(C.resPackGetResourceEmbedded(resName, nil, &size))

	if result != 0 {
		return fmt.Errorf("failed to load resource with error: %#x", uint(result))
	}

	buf := C.malloc(C.size_t(size))
	defer C.free(buf)

	result = uint(C.resPackGetResourceEmbedded(resName, &buf, &size))

	if result != 0 {
		return fmt.Errorf("failed to load resource with error: %#x", result)
	}

	compressedData := cBufToArray(buf, uint(size))
	data, err := uncompress(compressedData)

	if err != nil {
		return err
	}

	return respack.unmarshal(data)
}

func (respack *Respack) EmbedIntoExe(filename string) error {
	data, err := respack.marshal()

	if err != nil {
		return err
	}

	compressed, err := compress(data)

	if err != nil {
		return err
	}

	filenameC := C.CString(filename)
	defer C.free(unsafe.Pointer(filenameC))
	resourceC := C.CString("respack")
	defer C.free(unsafe.Pointer(resourceC))

	result := C.resPackAddResource(filenameC, resourceC, C.RT_RCDATA, unsafe.Pointer(&compressed[0]), C.uint(len(compressed)))

	if result != 0 {
		return fmt.Errorf("failed to add resource with error: %#x", uint(result))
	}

	return nil
}

func EmbedManifest(filename string, manifest string) error {
	manifestC := C.CString(manifest)
	defer C.free(unsafe.Pointer(manifestC))
	filenameC := C.CString(filename)
	defer C.free(unsafe.Pointer(filenameC))

	result := uint(C.resPackAddResource(
		filenameC,
		C.CREATEPROCESS_MANIFEST_RESOURCE_ID,
		C.RT_MANIFEST,
		unsafe.Pointer(manifestC),
		C.uint(C.strlen(manifestC))))

	if result != 0 {
		return fmt.Errorf("failed to add manifest with error %#x", result)
	}
	return nil
}